﻿using Ninject;
using System;
using System.Linq;
using System.Reflection;
using UMG_Technical_Test.Features.Contract.Models;

namespace UMG_Technical_Test
{
    class Program
    {
        static StandardKernel kernel;

        static void Main(string[] args)
        {
            kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            ContractModel contracts = kernel.Get<ContractModel>();

            while(true)
            {
                Console.WriteLine("Enter q to quit or enter to find active music contracts for a supplier.");
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.Q)
                    break;
                if (key.Key != ConsoleKey.Enter)
                    continue;
                // get delivery partner name
                Console.WriteLine("Enter Delivery Partner Name");
                string partnerName = Console.ReadLine();
                Console.WriteLine("Enter Effective Date");
                string effectiveDate = Console.ReadLine();
                // find music contracts applicable to this partner
                var partnerContract = contracts.PartnerContracts.FirstOrDefault(x => x.Partner == partnerName);
                // find music with that type
                var music = contracts.MusicContracts.Where(x => x.Usage.Contains(partnerContract.Usage));
                foreach(var song in music)
                {
                    Console.WriteLine(song.ToString());
                }
            }

            Console.WriteLine("Hello World!");
            Console.ReadKey();
        }
    }
}
