﻿using System;
using System.Collections.Generic;
using System.Text;
using UMG_Technical_Test.Features.Contract.Entities;
using UMG_Technical_Test.Features.Contract.Repository;

namespace UMG_Technical_Test.Features.Contract.Models
{
    class ContractModel
    {
        public ContractModel(PartnerContractRepository partnerContractRepository, MusicContractRepository musicContractRepository)
        {
            PartnerContracts = partnerContractRepository.Load();
            MusicContracts = musicContractRepository.Load();
        }

        public List<MusicContractEntity> MusicContracts { get; private set; }

        public List<PartnerContractEntity> PartnerContracts { get; private set; }
    }
}
