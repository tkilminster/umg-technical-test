﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UMG_Technical_Test.Features.Contract.Entities;

namespace UMG_Technical_Test.Features.Contract.Repository
{
    /// <summary>
    /// Notes.
    /// 
    /// There ought to be a real piped file repository for deserializing these entities.
    /// </summary>
    class PartnerContractRepository
    {
        public List<PartnerContractEntity> Load()
        {
            List<PartnerContractEntity> result = new List<PartnerContractEntity>();
            var file = new FileInfo("PartnerContractRepository.txt");
            string fileContent = string.Empty;
            using (FileStream stream = file.OpenRead())
            using (StreamReader sr = new StreamReader(stream, Encoding.UTF8))
                fileContent = sr.ReadToEnd();
            string[] contracts = Regex.Split(fileContent, "\r\n|\r|\n");
            // skip the first, it's the headings
            foreach (string contract in contracts.Skip(1))
            {
                PartnerContractEntity entity = new PartnerContractEntity();
                string[] content = contract.Split('|');
                // position determines property
                entity.Partner = content[0];
                entity.Usage = content[1];
                // add to results
                result.Add(entity);
            }
            return result;
        }
    }
}
