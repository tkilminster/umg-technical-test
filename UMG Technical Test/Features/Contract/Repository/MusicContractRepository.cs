﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UMG_Technical_Test.Features.Contract.Entities;

namespace UMG_Technical_Test.Features.Contract.Repository
{
    /// <summary>
    /// Notes.
    /// 
    /// There ought to be a real piped file repository for deserializing these entities.
    /// </summary>
    class MusicContractRepository
    {
        public List<MusicContractEntity> Load()
        {
            List<MusicContractEntity> result = new List<MusicContractEntity>();
            var file = new FileInfo("MusicContractRepository.txt");
            string fileContent = string.Empty;
            using (FileStream stream = file.OpenRead())
            using (StreamReader sr = new StreamReader(stream, Encoding.UTF8))
                fileContent = sr.ReadToEnd();
            string[] contracts = Regex.Split(fileContent, "\r\n|\r|\n");
            // skip the first, it's the headings
            foreach(string contract in contracts.Skip(1))
            {
                MusicContractEntity entity = new MusicContractEntity();
                string[] content = contract.Split('|');
                // position determines property
                entity.Artist = content[0];
                entity.Title = content[1];
                entity.Usage = content[2].Split(',');
                entity.StartDate = content[3];
                if (content.Length == 5)
                    entity.EndDate = content[4];
                // add to results
                result.Add(entity);
            }
            return result;
        }
    }
}
