﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UMG_Technical_Test.Features.Contract.Entities
{
    class PartnerContractEntity
    {
        public string Partner { get; set; }

        public string Usage { get; set; }
    }
}
