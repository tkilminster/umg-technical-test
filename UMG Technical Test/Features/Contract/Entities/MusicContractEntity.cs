﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UMG_Technical_Test.Features.Contract.Entities
{
    class MusicContractEntity
    {
        public string Artist { get; set; }

        public string Title { get; set; }

        public string[] Usage { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public override string ToString()
        {
            return Artist + '|' + Title + '|' + Usage + '|' + StartDate + '|' + EndDate;
        }
    }
}
