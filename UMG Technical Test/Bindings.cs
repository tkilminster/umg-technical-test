﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;
using UMG_Technical_Test.Features.Contract.Models;
using UMG_Technical_Test.Features.Contract.Repository;

namespace UMG_Technical_Test
{
    public class Bindings : NinjectModule
    {
        /// <summary>
        /// Setup Ninject DI bindings here
        /// </summary>
        public override void Load()
        {
            Bind<ContractModel>().To<ContractModel>().InSingletonScope();
            Bind<MusicContractRepository>().To<MusicContractRepository>();
            Bind<PartnerContractRepository>().To<PartnerContractRepository>();
        }
    }
}
